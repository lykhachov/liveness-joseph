const gulp = require('gulp'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    babel = require('gulp-babel'),
    browserSync = require('browser-sync').create(),
    yargs = require('yargs'),
    cleanCss = require('gulp-clean-css'),
    gulpif = require('gulp-if'),
    newer = require('gulp-newer');

//To test the production: gulp hello --prod=true
const PRODUCTION = yargs.argv.prod;
const buildFolder = './build/';
const srcFolder = 'dev/';
const path = {
    build: {
        js: buildFolder + 'static/js/',
        style: buildFolder + 'static/css/',
        root: buildFolder,
        img: buildFolder,
        fonts: buildFolder + 'static/fonts/',
        html: buildFolder
    },
    src: {
        js: srcFolder + 'static/js/main.js',
        jsLib: srcFolder + 'static/js/libs.js',
        style: srcFolder + 'static/scss/*.scss',
        styleLib: srcFolder + 'static/scss/style-libs.css',
        img: srcFolder + '**/**/*.{JPG,jpg,png,gif,svg,mp4}',
        fonts: srcFolder + 'static/fonts/**/*.*',
        html: srcFolder + '*.html'
    },
    watch: {
        js: srcFolder + 'static/js/**/*.js',
        style: srcFolder + 'static/scss/**/*.scss',
        img: srcFolder + 'static/img/**/*.*',
        fonts: srcFolder + 'static/fonts/**/*.*',
        html: srcFolder + '**/*.html'
    }
};

gulp.task('html', function () {
    return gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(browserSync.stream())
});
gulp.task('css-libs', function () {
    return gulp.src(path.src.styleLib)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.style))
});

gulp.task('js-libs', function () {
    return gulp.src(path.src.jsLib)
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
});
gulp.task('js', function () {
    return gulp.src(path.src.js)
        .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
        .pipe(rigger())
        .pipe(babel({presets: ['env'], plugins: ['@babel/plugin-transform-classes','@babel/plugin-proposal-object-rest-spread','@babel/plugin-transform-destructuring']}))
        .pipe(uglify())
        .pipe(gulpif(PRODUCTION, uglify()))
        .pipe(gulpif(!PRODUCTION, sourcemaps.write()))
        .pipe(gulp.dest(path.build.js))
        .pipe(browserSync.stream())
});
gulp.task('css', function () {
    return gulp.src(path.src.style)
        .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
        .pipe(sass().on('error', sass.logError))
        .pipe(prefixer())
        .pipe(gulpif(PRODUCTION, cleanCss({compatibility: 'ie11'})))
        .pipe(gulpif(!PRODUCTION, sourcemaps.write()))
        .pipe(gulp.dest(path.build.style))
        .pipe(browserSync.stream())
});
gulp.task('imageMin', function () {
    return gulp.src(path.src.img)
        .pipe(newer(path.build.img))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img));
});
gulp.task('fonts', function () {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});
gulp.task('watcher', function () {
    gulp.watch(path.watch.html).on('change', gulp.series('html'));
    gulp.watch(path.watch.style).on('change', gulp.series('css'));
    gulp.watch(path.watch.js).on('change', gulp.series('js'));
    gulp.watch(path.watch.img).on('change', gulp.series('imageMin'));
});
gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "./build/"
        }
    });
});
//npm run start for dev and npm run build for build
gulp.task('default', gulp.series(
    gulp.parallel('html', 'imageMin', 'css-libs', 'css', 'js','js-libs', 'fonts', 'browser-sync','watcher'),
));
/*to run build task from gulp tasks hover on it, click RMB -> edit 'build' settings -> Arguments: --prod
or just: npm run build
*/
gulp.task('build', gulp.series(
    gulp.parallel('html', 'imageMin', 'css-libs', 'css', 'js','js-libs', 'fonts')
));
// gulp.task('default', gulp.series(
//   gulp.parallel('imageMin', 'css-libs', 'js-libs', 'css', 'js', 'fonts', 'php', 'wp-main-css'),
//   gulp.parallel('watcher')
// ));
